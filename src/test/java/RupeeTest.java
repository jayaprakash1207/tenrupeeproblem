import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RupeeTest {
    @Test
    public void shouldCheckIfTheTwoTenRupeesAreTheSame() {
        Rupee firstTenRupee = new Rupee(1979,10);
        Rupee secondTenRupee = new Rupee(2001,10);
        assertNotSame(firstTenRupee, secondTenRupee);
    }

    @Test
    public void shouldCheckIfTwoTenRupeesAreEqual() {
        Rupee firstTenRupee = new Rupee(1999,10);
        Rupee secondTenRupee = new Rupee(2000,10);
        int valueOfTheFirstTenRupee = firstTenRupee.value;
        int valueOfTheSecondTenRupee = secondTenRupee.value;
        assertEquals(valueOfTheFirstTenRupee,valueOfTheSecondTenRupee,"The value of first ten rupee should be equal to the value of second ten rupee");
    }

}